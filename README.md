Сайт СКИБ КНИТ (ФОИТ)
=====================

Сайт развернут по адресу https://csit.mephi.ru/

Сайт генерируется из исходного кода в этом репозитории при помощи
[Jekyll](https://jekyllrb.com/) с темой [Minimal
Mistakes](https://mmistakes.github.io/minimal-mistakes/).

Содержимое страниц сайта находится в папке [`_pages`](_pages). Правки следует
вносить непосредственно в файлы страниц, к примеру, через [Web
IDE](https://gitlab.com/-/ide/project/skibcsit/skibcsit.gitlab.io/tree/master/-/_pages/about.md/),
и сохранять их в виде [Merge
Request](https://docs.gitlab.com/ee/user/project/merge_requests/). Когда
изменения будут внесены в ветвь `master`, они будут развернуты на основном
сервере.

Результат сборки измененной версии можно просмотреть в артефактах
соответствующего MR.

Общее описание процесса разработки см. на https://gitlab.com/psttf/core-docs/
