---
permalink: /directions/foit/
sidebar:
  nav: "directions"
---

Фундаментальные основы информационных технологий
================================================

**[Программа
направления](https://www.dropbox.com/s/8zczo7281apgnuo/2012-FIVT.pdf)**

**Руководитель:** д. т. н., проф., ACM Senior Member [Вольфенгаген
Вячеслав Эрнстович](https://www.wolfengagen.mephi.ru/)

- Имеет звание ACM Senior Member от Association for Computing Machinery (ACM)
  --- <http://www.acm.org>
- Член совета Общества цифровой информации и
  беспроводной коммуникации (The Society of Digital Information and
  WirelessCommunications, SDIWC) —
  <http://www.sdiwc.net>
- По проблематике читаемых дисциплин организована Международная конференция
  "Аппликативные Вычислительные Системы", председателем которой является по
  настоящее время. См.
    - АВС'2008 (URL <http://jurinfor.exponenta.ru/acs2008/> ),
    - АВС'2010 (URL <http://jurinfor.exponenta.ru/acs2010/> ),
    - АВС'2012 (URL <http://jurinfor.exponenta.ru/acs2012/> ).
- В их рамках организуется секция молодежных проектов (студенты и
  аспиранты).

**[Википедия](http://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%BB%D1%8C%D1%84%D0%B5%D0%BD%D0%B3%D0%B0%D0%B3%D0%B5%D0%BD,_%D0%92%D1%8F%D1%87%D0%B5%D1%81%D0%BB%D0%B0%D0%B2_%D0%AD%D1%80%D0%BD%D1%81%D1%82%D0%BE%D0%B2%D0%B8%D1%87)**

**Монографии:**

- Вольфенгаген В.Э. Парадигма функционального программирования. / Под ред.
  к.т.н. Л.Ю. Исмаиловой. -- М.: АО Центр ЮрИнфоР,2012. — 96 с.
- Wolfengagen V.E. *Applicative computing. Its quarks, atoms and molecules*. --
  M.: Center JurinfoR, 2010. — 62 с . ISBN 978-5-89158-177-7.;
- Вольфенгаген В. Э.  *Аппликативные вычислительные технологии. Готовые решения
  для инженера, преподавателя, аспиранта, студента* М.: Институт«ЮрИнфоР-МГУ»,
  2009\. 64 с.  ISBN 978-5-903678-04-4.;
- Вольфенгаген В.Э. *Комбинаторная логика в программировании. Вычисления с
  объектами в примерах и задачах.* -- 3-е изд.дополн. и перераб. М.: Институт
  "ЮрИнфоР-МГУ", 2008. —Х+384 с. ISBN978-5-91329-013-7; — 2-е изд. — М.: АО
  \"Центр ЮрИнфоР\", 2003. — vi +336 с. ISBN 5-89158-101-9; М.:МИФИ, 1994. --
  204 с.;
- Вольфенгаген В.Э. *Методы и средства вычислений с объектами.Аппликативные
  вычислительные системы.* — М.: JurInfoR Ltd., АО \"Центр ЮрИнфоР\", 2004. —
  xvi+789 с. ISBN 5-89158-100-0 (\*\*\*Книга отмечена дипломом Фонда развития
  отечественного образования 2005 г.); — грант РФФИ 03-01-14055
- Wolfengagen V.E. *Combinatory logic in programming. Computations with objects
  through examples and exercises.* — 2-nd ed. — M.:\"Center JurInfoR\" Ltd.,
  2003\. — x+337 с. ISBN 5-89158-101-9;
- Вольфенгаген В.Э. *Конструкции языков программирования.Приемы описания.* — М.:
  АО \"Центр ЮрИнфоР\", 2001. — 276 с. ISBN  5-89158-079-9; — грант РФФИ
  01-01-14068
- Вольфенгаген В.Э. *Логика. Конспект лекций: техника рассуждений*. 2-е изд.,
  дополн. и перераб. — М.: АО \"Центр ЮрИнфоР\", 2004. —229 с. ISBN
  5-89158-135-3 (\*\*\*Книга стала победителем в номинации "Лучшее учебное
  издание по точным наукам" на III   Общероссийском конкурсе учебных изданий для
  высших учебных заведений "Университетская книга 2006"); — М.:АО
  \"Центр ЮрИнфоР\", 2001. — 137 с.
- Вольфенгаген В.Э. *Категориальная абстрактная машина. Конспект лекций:
  введение в вычисления.* — 2-е изд. — М.: АО \"Центр ЮрИнфоР\", 2002. — 96 с.
  ISBN 5-89158-102-7; М.: МИФИ, 1993. -- 96 с.
- Стогний А.А., Вольфенгаген В.Э., Кушниров В.А., Саркисян В.И., Араксян В.В.,
  Шитиков А.А. *Проектирования интегрированных баз данных*. — Киев: \"Технiка\",
  1987\. — 138 с. (в соавторстве);
- Вольфенгаген В.Э., Яцук В.Я. *Аппликативные вычислительные системы и
  концептуальный метод проектирования систем знаний*. — Министерство Обороны
  СССР, 1987. — 256 с. (в соавторстве);
- Вольфенгаген В.Э., Кузин Л.Т., Саркисян В.И. *Реляционные методы
  проектирования банков данных.* — Киев: \"Вищашкола\", 1979. — 192 с. (в
  соавторстве).
