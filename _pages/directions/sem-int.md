---
permalink: /directions/sem-int/
sidebar:
  nav: "directions"
---

Семантические методы интеграции программных систем
==================================================

**Руководитель:** к. т. н., доц. Шапкин Павел Александрович

**Email:** <pashapkin@mephi.ru>

Программа исследований
----------------------

Общей целью исследований является автоматизация решения следующих задач:

- автоматизация интеграции компонентов на различных языках, платформах (JVM, JS,
  Native, Android и др.) и протоколах взаимодействия (REST и др.);
- верификация API и архитектур программных систем и баз данных;
- автоматизация развертывания систем на различных облачных и частных платформах,
  в т. ч. на основе контейнеризации (Docker, Kubernetes, Helm);
- построение пользовательских интерфейсов, MVC и SPA веб-приложений, управляемых
  тИповыми моделями предметных областей.

Исследования опираются на методы аппликативных вычислительных систем, теорию
типов, подходы концептуального моделирования и функционального программирования.
Используются языки и технологии [Scala](https://www.scala-lang.org/),
[Scala.js](https://www.scala-js.org/), [cats](https://typelevel.org/cats/),
[cats-effect](https://typelevel.org/cats-effect/) и
[Coq](https://coq.inria.fr/). Для поддержки взаимодействия на естественном языке
используются технологии машинного обучения и перевода.

С целью оснащения учебного процесса, а также более глубокого погружения в
структуру изучаемых формализмов проводится разработка автоматизированных систем
изучения и лабораторных комплексов.

Ссылки
------

Начальное представление об областях исследования можно получить по следующим
ссылкам:

- [Теория типов](http://en.wikipedia.org/wiki/Type_theory)
- [Соответствие Карри ---
  Говарда](https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%81%D1%82%D0%B2%D0%B8%D0%B5_%D0%9A%D0%B0%D1%80%D1%80%D0%B8_%E2%80%94_%D0%A5%D0%BE%D0%B2%D0%B0%D1%80%D0%B4%D0%B0)
- [Аппликативные вычислительные
  системы](https://ru.wikipedia.org/wiki/%D0%90%D0%BF%D0%BF%D0%BB%D0%B8%D0%BA%D0%B0%D1%82%D0%B8%D0%B2%D0%BD%D0%BE%D0%B5_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5)
- [Зависимые типы](http://en.wikipedia.org/wiki/Dependent_types)

Публикации
----------

1. P. A. Shapkin, D. Saenko. A Type-Safe Approach to Identifying and Merging
   Changes in Complex Information Objects // Procedia Computer Science. 2018.
   Vol. 145. P. 453–457.
1. S. V. Zykov, P. A. Shapkin et al. Adding agility to Enterprise Process and Data
   Engineering // Journal of Systemics. 2016. Vol. 14, № 2. P. 72–77.
1. S. I. Ruban, P. A. Shapkin, S. V. Zykov. A Dynamic Editor of Typed Data
   Transformations // Procedia Computer Science. 2016. Vol. 96. P. 961–967.
1. A. I. Domracheva, P. A. Shapkin. Type-theoretic Means for Querying
   Heterogeneous Data Sources Available via Cloud APIs // Procedia Computer
   Science. 2016. Vol. 96. P. 951–960.
1. S. V. Zykov, P. A. Shapkin et al. Agile Enterprise Process and Data
   Engineering via Type-Theory Methods // ISKO-Maghreb: Knowledge Organization
   in the perspective of Digital Humanities: Researches and Applications
   (ISKO-Maghreb), 2015 5th International Symposium. 2015.
1. P. A. Shapkin et al. Towards the Automated Business Process Building by Means
   of Type Theory // Proceedings of the 7th International Conference on
   Subject-Oriented Business Process Management. New York, NY, USA: ACM, 2015.
   P. 7:1-7:8.
1. P. A. Shapkin, G. Pomadchin. A Type-Theoretic Approach to Cloud Data Integration
   // Proceedings of the 11th International Conference on Web Information
   Systems and Technologies WEBIST. INSTICC Press, 2015. P. 164–169.
1. P. A. Shapkin, L. D. Shumsky. A Language for Transforming the RDF Data on the
   Basis of Ontologies // Proceedings of the 11th International Conference on
   Web Information Systems and Technologies WEBIST. INSTICC Press, 2015. P.
   504–511.
1. P. A. Shapkin, A. Demchenko. Ontology-Based Type-System Integrated Tools to
   Infer Facts about Information Objects for .NET Platform // Autom. Doc. Math.
   Linguist. 2014. Vol. 48, № 2. P. 52–59.
1. Л. Д. Шумский et al. Семантическая разметка бизнес-процессов в интеграционной
   автоматизации // Информационные технологии в проектировании и производстве.
   2014\. № 1 (153). P. 32–39.
1. П. А. Шапкин, А. П. Демченко. Средства вывода фактов о типовых информационных
   объектах // Научно-техническая информация. Серия 2: Информационные процессы и
   системы. 2014. № 3. P. 14–23.
1. A. Gromoff et al. Automatic Business Process Model Assembly on the Basis of
   Subject-Oriented Semantic Process Mark-up // Proceedings of the International
   Conference on e-Business (ICE-B 2014). INSTICC Press, 2014. P. 158–164.
1. P. A. Shapkin, Shumsky L., V. Wolfengagen. Usage of Semantic Transformations
   in B2B Integration Solutions // Proceedings of the International Conference
   on e-Business (ICE-B 2014). INSTICC Press, 2014. P. 152–157.
2. П. А. Шапкин, В. В. Рословцев, Л. Д. Шумский, В. Э. Вольфенгаген. Методы
   и средства генерации текстовых документов на основе концептуальной
   модели картографических данных // Научная сессия НИЯУ МИФИ-2013.
   Аннотации докладов. Т. 2. --- М.: НИЯУ МИФИ, 2013. --- С. 301.
2. П. А. Шапкин, В. В. Рословцев. Двухуровневая концептуализация предметных
   областей средствами дескрипционной логики // 3-я международной конференции по
   аппликативным вычислительным системам (АВС'2012). Сб. трудов. — М.: НОУ
   Институт Актуального образования «ЮрИнфоР-МГУ», 2012. --- С. 218--223\.
2. П. А. Шапкин. Язык шаблонного преобразования RDF-данных на основе онтологий
   // 2-я Международная научно-практическая конференция «Веб-программирование и
   Интернет-технологии WebConf-2012». Сб. трудов. — Минск, 2012.
2. П. А. Шапкин, А. С. Бохуленков, В. Э. Вольфенгаген. Платформа интеграции и
   композиции SaaS-приложений // XV Международная телекоммуникационная
   конференция молодых ученых и студентов «Молодежь и наука».Тезисы докладов. Ч.
   3\. — М.: НИЯУ МИФИ, 2012 --- С. 152--153.
2. А. П. Демченко, П. А. Шапкин. Аппликативные средства вычисления
   ISA-иерархии концептов в выразительных дескрипционных логиках //
   XV Международная телекоммуникационная конференция молодых ученых
   и студентов «Молодежь и наука».Тезисы докладов. Ч. 3. --- М.: НИЯУ МИФИ,
   2012\. --- С. 147--148.
2. А. В. Сахацкий, А. С. Бохуленков, П. А. Шапкин. Оптимизация поиска
   и обработки связанных данных // XV Международная
   телекоммуникационная конференция молодых ученых и студентов«Молодежь и
   наука». Тезисы докладов. Ч. 3. --- М.: НИЯУ МИФИ, 2012 --- С.
   43--44.
1. В. Э. Вольфенгаген, А. Д. Лаптев, В. Н. Назаров, В. В. Рословцев, П.
   А.Шапкин. Реляционные решения в реализации фундаментальных механизмов
   вычислений для аппликативных вычислительных технологий // Науч. сессия
   МИФИ-2011. Сб.науч. трудов. Т. 1 --- М.: МИФИ, 2011. --- С.
   131--133.
1. Климов В. В, Шапкин П. А., Климов В. П., Шумский Л. Д. Подход к построению
   интерфейса композиции веб-сервисов на основе семантических описаний //
   Вестник Воронежского государственного технического университета. Т. 6. ---
   Воронеж: ВГТУ, 2010. --- № 12. --- С. 152--157.
1. Шапкин П. А. Вычисления с концептами в аппликативном языке программирования.
   // Аппликативные вычислительные системы: Труды 2-й международной конференции
   по аппликативным вычислительным системам (АВС'2010) /Под ред. В. Э.
   Вольфенгагена. --- М.: НОУ Институт Актуального образования«ЮрИнфоР-МГУ»,
   2010\. --- С. 205--213.
1. Шумский Л. Д., Шапкин П. А. Преобразование данных в формате RDF
   на основе веб-онтологий // Аппликативные вычислительные системы: Труды 2-й
   международной конференции по аппликативным вычислительным системам (АВС'2010)
   / Под ред. В. Э. Вольфенгагена. --- М.: НОУ Институт Актуального образования
   «ЮрИнфоР-МГУ», 2010. --- С. 195--204.
1. Климов В. В, Ульянов М. Е., Шапкин П. А., Кудинов М. А., Климов В. П.Система
   описания и выполнения композиций семантических веб-сервисов //Информационные
   технологии в проектировании и производстве. --- 2010. --- 4. ---
   С.64--69.
1. Шапкин П. А. Модели и методы разработки Веб-приложений на основе
   онтологии предметной области // Информационные технологии. --- М.: Новые
   технологии, 2010.--- № 2. --- С. 13--18.
1. Шапкин П. А. Разработка веб-приложений на основе онтологий и семантических
   шаблонов // XIII Международная телекоммуникационная конференция студентов и
   молодых ученых«Молодежь и наука». Тезисы докладов. Ч. 2\. --- М.: НИЯУ МИФИ,
   2010 --- С. 28-29.
1. Shapkin P. A. Developing Web Information Systems On the Basis of Domain
   Ontology // Proceedings of the Workshop on Computer Science and Information
   Technologies (CSIT'2009). Vol. 1. --- Crete,
   Greece, 2009. --- Pp. 120--123.
1. Шапкин П. А. Использование онтологий при разработке
   веб-приложений,настраиваемых на предметную область. // Информационные
   технологии и вычислительные системы. --- 2009. --- № 2. --- С.
   44--50.
1. Шапкин П. А. Разработка службы доступа к системе классификационных схем на
   основе формальной модели связанных понятий. --- М., 2009. --- 60 с., ил. ---
   Деп. в ВИНИТИ РАН 28.09.09, № 575-В2009.
1. Shapkin P. A. Potential of Using Ontologies as Models for Web Application
   Development // Proceedings of the Workshop on ComputerScience and Information
   Technologies (CSIT'2008). Vol. 2. — Ufa, 2008. --- P. 249--252.
1. Шапкин П. А. Описание Web-приложений с использованием аппликативного подхода
   // Труды конференции по аппликативным вычислительным системам (АВС'2008) ---
   Москва, 2008. --- С. 20--21.
1. Шапкин П. А. Поиск документов в разнородных источниках на основе системы
   классификационных схем // Науч. сессия МИФИ-2008. Сб. науч. трудов. Т. 3 ---
   М.:МИФИ, 2008. --- С. 120--121.
1. Shapkin P. A. Web Service for Accessing Classification Scheme Mappings //
   Proceedings of CSIT\'2007. Vol. 1. --- Ufa, 2007. ---
   P. 105--109.
1. Shapkin P., Shapkin A. Software tools for navigation in document databases.
   Developing of InformationNavigation Service Based on Classification Schemes
   // Proceedings of theWorkshop Third International Conference on Web
   Information Systems andTechnologies (WebIST2007). Volume "Web Interfaces and
   Applications" (WIA). --- INSTICCPress, 2007. --- P. 455--458.
1. Шапкин П. А. Применение технологий ASP.NET и Семантического Интернета для
   обеспечения доступа к системе классификационных схем // Научно-техническая
   информация, серия 2. --- 2006. --- №5. --- С. 20--26.
