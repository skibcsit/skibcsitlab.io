---
permalink: /directions/sem-int/en/
sidebar:
  nav: "directions"
---

Semantic Methods for Software Systems Integration
=================================================

**Head:** Ph. D., Assoc. Prof. Pavel Shapkin

**Email:** <pashapkin@mephi.ru>

Research program
----------------

The overall goal of research is to automate the solution of the following tasks:

- search and selection of software components by their specifications;
- generation of component integration-specific code;
- software system architectures verification;
- deployment of systems on various cloud and private platforms, including
  container-based;
- building of user interfaces, MVC and SPA web applications driven by typed
  domain models.

The research is based on the methods of applicative computing systems, type
theory, conceptual modeling and functional programming. Machine learning and
translation technologies are used to support natural language interaction.

In order to equip the educational process and deepen immersion in the structure
of the formalisms studied, the development of automated study systems and
laboratory complexes is carried out.

Links
-----

An overview of the research areas can be obtained from the following links:

- [Type Theory](http://en.wikipedia.org/wiki/Type_theory)
- [Curry–Howard correspondence](https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence)
- [Applicative Computing
  systems](https://en.wikipedia.org/wiki/Applicative_computing_systems)
- [Dependent Types](http://en.wikipedia.org/wiki/Dependent_types)

Publications
------------

1. P. A. Shapkin, D. Saenko. A Type-Safe Approach to Identifying and Merging
   Changes in Complex Information Objects // Procedia Computer Science. 2018.
   Vol. 145. P. 453–457.
1. S. V. Zykov, P. A. Shapkin et al. Adding agility to Enterprise Process and Data
   Engineering // Journal of Systemics. 2016. Vol. 14, № 2. P. 72–77.
1. S. I. Ruban, P. A. Shapkin, S. V. Zykov. A Dynamic Editor of Typed Data
   Transformations // Procedia Computer Science. 2016. Vol. 96. P. 961–967.
1. A. I. Domracheva, P. A. Shapkin. Type-theoretic Means for Querying
   Heterogeneous Data Sources Available via Cloud APIs // Procedia Computer
   Science. 2016. Vol. 96. P. 951–960.
1. S. V. Zykov, P. A. Shapkin et al. Agile Enterprise Process and Data
   Engineering via Type-Theory Methods // ISKO-Maghreb: Knowledge Organization
   in the perspective of Digital Humanities: Researches and Applications
   (ISKO-Maghreb), 2015 5th International Symposium. 2015.
1. P. A. Shapkin et al. Towards the Automated Business Process Building by Means
   of Type Theory // Proceedings of the 7th International Conference on
   Subject-Oriented Business Process Management. New York, NY, USA: ACM, 2015.
   P. 7:1-7:8.
1. P. A. Shapkin, G. Pomadchin. A Type-Theoretic Approach to Cloud Data Integration
   // Proceedings of the 11th International Conference on Web Information
   Systems and Technologies WEBIST. INSTICC Press, 2015. P. 164–169.
1. P. A. Shapkin, L. D. Shumsky. A Language for Transforming the RDF Data on the
   Basis of Ontologies // Proceedings of the 11th International Conference on
   Web Information Systems and Technologies WEBIST. INSTICC Press, 2015. P.
   504–511.
1. P. A. Shapkin, A. Demchenko. Ontology-Based Type-System Integrated Tools to
   Infer Facts about Information Objects for .NET Platform // Autom. Doc. Math.
   Linguist. 2014. Vol. 48, № 2. P. 52–59.
1. A. Gromoff et al. Automatic Business Process Model Assembly on the Basis of
   Subject-Oriented Semantic Process Mark-up // Proceedings of the International
   Conference on e-Business (ICE-B 2014). INSTICC Press, 2014. P. 158–164.
1. P. A. Shapkin, Shumsky L., V. Wolfengagen. Usage of Semantic Transformations
   in B2B Integration Solutions // Proceedings of the International Conference
   on e-Business (ICE-B 2014). INSTICC Press, 2014. P. 152–157.
1. Shapkin P. A. Developing Web Information Systems On the Basis of Domain
   Ontology // Proceedings of the Workshop on Computer Science and Information
   Technologies (CSIT'2009). Vol. 1. --- Crete,
   Greece, 2009. --- Pp. 120--123.
1. Shapkin P. A. Potential of Using Ontologies as Models for Web Application
   Development // Proceedings of the Workshop on ComputerScience and Information
   Technologies (CSIT'2008). Vol. 2. — Ufa, 2008. --- P. 249--252.
1. Shapkin P. A. Web Service for Accessing Classification Scheme Mappings //
   Proceedings of CSIT\'2007. Vol. 1. --- Ufa, 2007. ---
   P. 105--109.
1. Shapkin P., Shapkin A. Software tools for navigation in document databases.
   Developing of InformationNavigation Service Based on Classification Schemes
   // Proceedings of theWorkshop Third International Conference on Web
   Information Systems andTechnologies (WebIST2007). Volume "Web Interfaces and
   Applications" (WIA). --- INSTICCPress, 2007. --- P. 455--458.
